<?php

/**
 * Implements hook_filter_info().
 * @return array
 */
function feature_wysiwyg_editor_filter_info() {
  $filters = array();
  $filters['feature_wysiwyg_editor_video'] = array(
    'title' => t('Video URL Convertor'),
    'description' => t('Convert Youtube URLS to actual video embeds.'),
    'process callback' => '_feature_wysiwyg_editor_process',
    'weight' => -11
  );

  $filters['feature_wysiwyg_editor_align'] = array(
    'title' => t('Image Alignment CSS Classes'),
    'description' => t('Add a class to floated images.'),
    'process callback' => '_feature_wysiwyg_editor_filter_process',
    'weight' => -12
  );

  $filters['feature_wysiwyg_editor_lists'] = array(
    'title' => t('List Style CSS Classes'),
    'description' => t('Add a class to allow for different list types.'),
    'process callback' => '_feature_wysiwyg_editor_filter_list_process',
    'weight' => -12
  );

  return $filters;
}

/**
 * Process callback for list items.
 * @param $text
 * @param $filter
 * @param $format
 * @param $langcode
 * @param $cache
 * @param $cache_id
 * @return mixed
 */
function _feature_wysiwyg_editor_filter_list_process($text, $filter, $format, $langcode, $cache, $cache_id)  {

  $matches = array();
  $pattern = '/<ol[^>]*list-style-type:\s*(upper-roman|lower-roman|lower-alpha|upper-alpha|decimal);[^>]*>/i';
  preg_match_all($pattern, $text, $matches);

  foreach ($matches[0] as $key => $html) {
    // Add to an existing class attribute or set the class attribute.
    if (strpos($html, 'class="') !== FALSE) {
      $replace = str_replace('class="', 'class="list-' . $matches[1][$key] . ' ', $html);
    }
    else {
      $replace = str_replace('<ol', '<ol class="list-' . $matches[1][$key] . '"', $html);
    }
    $text = str_replace($html, $replace, $text);
  }

  return $text;
}

/**
 * Implements filter process callback.
 */
function _feature_wysiwyg_editor_process($text, $filter) {

  // save all urls and put a token in the body so we can put them back in later.
  $link_search = "#<a(.*?)</a>#si";

  preg_match_all($link_search, $text, $link_matches);

  foreach ($link_matches[0] as $key => $value) {
    $text = str_replace($value, '*******' . $key . '*******', $text);
  }

  /* Replace youtube video urls  */
  $youtube_search = '/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i';
  $youtube_replace = '<iframe class="video youtube" title="Youtube Video $1" src="//www.youtube.com/embed/$1"></iframe>';
  $output = preg_replace($youtube_search, $youtube_replace, $text);

  /* Replace vimeo video urls  */
  $vimeo_search = '/https?:\/\/(www.vimeo|vimeo)\.com(\/|\/clip:)(\d+)(.*?)/';
  $vimeo_replace = '<iframe class="video vimeo" title="Vimeo Video $3" src="//player.vimeo.com/video/$3"></iframe>';
  $output = preg_replace($vimeo_search, $vimeo_replace, $output);

  /* Replace youtube video urls that use the youtu.be format  */
  $youtube_search = '/\s*[a-zA-Z\/\/:\.]*youtu.be\/([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i';
  $youtube_replace = '<iframe class="video youtube" title="Youtube Video $1" src="//www.youtube.com/embed/$1"></iframe>';
  $output = preg_replace($youtube_search, $youtube_replace, $output);

  foreach ($link_matches[0] as $key => $value) {
    $output = str_replace('*******' . $key . '*******', $link_matches[0][$key], $output);
  }

  return $output;
}


/**
 * Filter process callback.
 */
function _feature_wysiwyg_editor_filter_process($text, $filter, $format, $langcode, $cache, $cache_id) {
  // Find images floated with inline styles, examine the float attribute and add
  // a class name of "alignleft" or "alignright" accordingly.
  $matches = array();
  $pattern = '/<img[^>]*float:\s*(left|right);[^>]*>/i';
  preg_match_all($pattern, $text, $matches);

  foreach ($matches[0] as $key => $html) {
    // Add to an existing class attribute or set the class attribute.
    if (strpos($html, 'class="') !== FALSE) {
      $replace = str_replace('class="', 'class="align' . $matches[1][$key] . ' ', $html);
    }
    else {
      $replace = str_replace('<img', '<img class="align' . $matches[1][$key] . '"', $html);
    }

    // Finally, replace the original image html with the new html containing the
    // alignleft/alignright class name.
    $text = str_replace($html, $replace, $text);
  }
  // replace all br tags with the xhtml couterpart
  $text = str_replace('<br>', '<br />', $text);

  $tags = array('p', 'h1', 'h2', 'h3', 'h4');

  foreach ($tags as $tag) {
    $pattern = '/<' . $tag . '[^>]*text-align:\s*center;[^>]*>/i';
    preg_match_all($pattern, $text, $matches);

    foreach ($matches[0] as $key => $html) {
      // Add to an existing class attribute or set the class attribute.
      if (strpos($html, 'class="') !== FALSE) {
        $replace = str_replace('class="', 'class="aligncenter ', $html);
      }
      else {
        $replace = str_replace('<' . $tag . '', '<' . $tag . ' class="aligncenter" ', $html);
      }

      // Finally, replace the original image html with the new html containing the
      // alignleft/alignright class name.
      $text = str_replace($html, $replace, $text);
    }
  }


  $pattern = '/<p[^>]*text-align:\s*right;[^>]*>/i';
  preg_match_all($pattern, $text, $matches);

  foreach ($matches[0] as $key => $html) {
    // Add to an existing class attribute or set the class attribute.
    if (strpos($html, 'class="') !== FALSE) {
      $replace = str_replace('class="', 'class="textalignright ', $html);
    }
    else {
      $replace = str_replace('<p', '<p class="textalignright" ', $html);
    }

    // Finally, replace the original image html with the new html containing the
    // alignleft/alignright class name.
    $text = str_replace($html, $replace, $text);
  }


  $pattern = '/<img[^>]*margin-left:\s*auto;\s*margin-right:\s*auto;[^>]*>/i';
  preg_match_all($pattern, $text, $matches);
  foreach ($matches[0] as $key => $html) {

    // Add to an existing class attribute or set the class attribute.
    if (strpos($html, 'class="') !== FALSE) {
      $replace = str_replace('class="', 'class="imgcenter ', $html);
    }
    else {
      $replace = str_replace('<img', '<img class="imgcenter" ', $html);
    }

    // Finally, replace the original image html with the new html containing the
    // imgcenter class name.
    $text = str_replace($html, $replace, $text);
  }


  return $text;
}
